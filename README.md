### It's my research analysis of video games.

### Contents:
#### 1 Downloading dataset
#### 2 Data preprocessing
##### 2.1 Web scrapping for filling na values
##### 2.2 Filling na values with median values
##### 2.3 Label encoding of categorical columns
##### 2.4 Filling na values with KNNimputer
#### 3 Exploratory data analysis
##### 3.1 Overall
##### 3.2 PC, Xbox and Playstation overview
##### 3.3 Top publishers and famous shooters overview
##### 3.4 Region differences and other interesting findings
#### 4) Hypothesis testing
